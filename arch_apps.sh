#!/bin/bash
clear

wm = i3-gaps i3blocks i3lock i3status
list_of_apps = chromium vim ranger

sudo pacman -S $wm --noconfirm --needed
sudo pacman -s $list_of_apps --noconfirm --needed

echo "Done!"
exit